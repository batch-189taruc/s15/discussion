// For data or text output into the browser console
console.log('Hello World!')
// single line comment ctrl + /
/* multi line comment ctrl + shift + / */
// syntax is the code that makes the statement
// variables and constants are used to contain data for various purposes 
// such as storage,  retrieval, and manipulation
let firstName = 'Jerome';
console.log(firstName);
let lastName = 'Musk';
console.log(lastName);
// reassigning value for "let" is okay
firstName = 'Elon'
console.log(firstName);
const colorOfTheSun = 'yellow';
console.log('The color of the sun is ' + colorOfTheSun);
// error happens when you try to re-assign value of const variable
// colorOfTheSun = 'red'
// console.log(colorOfTheSun)

let variableName = 'value'
variableName = 'New Value'
// no need to use "let" if we will reassign a value
// 6 types of data:
// string numbers boolean null undefined object

// DATA TYPES
// 1. String
let personName = 'Earl Diaz'

// 2. Number
let personAge = 15

// 3. Boolean true or false
let hasGirlfriend = false

// 4. Array
let hobbies = ['Cycling', 'Reading', 'Coding']

// 5. Object
let person = {
	personName: 'Earl Diaz',
	personAge: 15,
	hasGirlfriend: false,
	hobbies: ['Cycling', 'Reading', 'Coding']
}


// 6. Null
let wallet = null

console.log(personName)
console.log(personAge)
console.log(hasGirlfriend)
console.log(hobbies)
console.log(person)
console.log(wallet)


// to display single value of an object

console.log(person.personAge)

// to display from array
console.log(hobbies[0])